# Field Group Settings

## CONTENTS OF THIS FILE

 * Introduction
 * Installation
 * Requirements
 * Configuration

## INTRODUCTION

A field group formatter to hide settings unobtrusively on the form. Any fields within this group will hide in a panel that is toggled by a button. This button (a gear icon) will float to the right side of the form.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/field_group).

Submit bug reports and feature requests or track changes in the
[issue queue](https://www.drupal.org/project/issues/field_group).

## INSTALLATION

The installation of this module is like other Drupal modules. For further
information see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## REQUIREMENTS

You need module [Field Group](https://www.drupal.org/project/field_group) version 3.

## CONFIGURATION

- Go the the `Manage form display` tab of your entity and create a new field group (`Add field group`).
- Choose `Settings` as type of field group and input your label (e.g. 'Settings')
- On the next screen, select your options for the new field group and the roles that can access this field group.
- You find the new field group at the end of your screen in the "disabled" area.
- Move the new field group to the visible area.
- Place all fields for your field group under the field group item.
- Don't forget to save the whole form! 
